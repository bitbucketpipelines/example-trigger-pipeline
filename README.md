Mymath
=========
This package containes simple mymath for tests.

Installation
============

`pip install mymath`

Module mymath.core
========================

Functions
---------
`sum_of_two(a,b)`
:   Sum a value with b

    Args:
    	a (int): First value.
    	b (int): Second value.

    Returns:
    	Sum of a and b
