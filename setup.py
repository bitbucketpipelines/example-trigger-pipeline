from setuptools import setup


setup(
    name='mymath',
    version='0.0.1',
    packages=['mymath', ],
    url='https://bitbucket.org/akyrdan/example-trigger-pipeline/src',
    author='akyrdan',
    author_email='akyrdan@atlassian.com',
    description='This package containes simple mymath for tests',
    long_description=open('README.md').read(),
    install_requires=[]
)
